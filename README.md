# SolveOnline React Component Library


## Development

### Testing

```
yarn test
```

### Building

```
yarn build
```

### Storybook

To run a live-reload Storybook server on your local machine:

```
yarn storybook
```

To export your Storybook as static files:

```
yarn storybook:export
```

You can then serve the files under `storybook-static` using S3, GitHub pages, Express etc.

### Generating New Components

Included a handy NodeJS util file under `util` called `create-component.js`. Instead of copy pasting components to create a new component, you can instead run this command to generate all the files you need to start building out a new component. To use it:

```
yarn generate YourComponentName
```

This will generate:

```
/src
  /YourComponentName
    YourComponentName.tsx
    YourComponentName.stories.tsx
    YourComponentName.test.tsx
    YourComponentName.types.ts
    YourComponentName.scss
```

The default templates for each file can be modified under `util/templates`.

Don't forget to add the component to your `index.ts` exports if you want the library to export the component!

### Installing Component Library Locally

Let's say you have another project (`test-app`) on your machine that you want to try installing the component library into without having to first publish the component library. In the `test-app` directory, you can run:

```
yarn install ../so-ui-react-components-library
```

which will install the local component library as a dependency in `test-app`. It'll then appear as a dependency in `package.json` like:

```JSON
  ...
  "dependencies": {
    ...
    "react-component-library": "file:../so-ui-react-components-library",
    ...
  },
  ...
```

Your components can then be imported and used in that project.

## Publishing

```
yarn version --minor|major|patch
```


### Hosting via Bitbucket

You can install your library into other projects by running:

```
yarn add git+https://bitbucket.org/sojustin/so-ui-react-components-library.git#<tag>
```

## Usage

Usage of the component (after the library installed as a dependency into another project) will be:

```TSX
import React from "react";
import { TestComponent } from "so-ui-react-components-library";

const App = () => (
  <div className="app-container">
    <h1>Hello I'm consuming the component library</h1>
    <TestComponent theme="primary" />
  </div>
);

export default App;
```


### Using Component Library SASS Variables

For example, let's say you installed `so-ui-react-components-library` into your project. To use the exported variables/mixins, in a SASS file you would do the following:

```Sass
@import '~so-ui-react-components-library/build/scss/quick-website.scss';

.example-container {
    @include heading;

    color: $so-white;
}
```
