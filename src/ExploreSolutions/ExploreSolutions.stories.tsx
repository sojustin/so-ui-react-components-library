import React from "react";
import Component from "./ExploreSolutions";
import Alert from '@material-ui/lab/Alert';
import IconButton from '@material-ui/core/IconButton';
import Collapse from '@material-ui/core/Collapse';
import CloseIcon from '@material-ui/icons/Close';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';


export default {
    title: "Solve Online/Components/ExploreSolutions",
    component: Component
};

export const ExploreSolutions = () => {

    const [open, setOpen] = React.useState(false);
    const [isActive, setIsActive] = React.useState(false);
    const [disabled, setDisabled] = React.useState(false);

    return <React.Fragment>
        <FormGroup row>
            <FormControlLabel
                control={<Checkbox
                    onChange={(event) => setIsActive(event.target.checked)}
                    inputProps={{ 'aria-label': 'primary checkbox' }}
                />}
                label="Set active"
            />
            <FormControlLabel
                control={<Checkbox
                    onChange={(event) => setDisabled(event.target.checked)}
                    inputProps={{ 'aria-label': 'primary checkbox' }}
                />}
                label="Set disabled"
            />

        </FormGroup>
        <div className="row">
            <div className="col-4">
                <Component title="Supply" desc="Short description" img="01_Supply.svg" onClick={() => setOpen(true)} isActive={isActive} disabled={disabled}/>
            </div>
            <Collapse in={open}>
                <Alert
                    action={
                        <IconButton
                            aria-label="close"
                            color="inherit"
                            size="small"
                            onClick={() => {
                                setOpen(false);
                            }}
                        >
                            <CloseIcon fontSize="inherit" />
                        </IconButton>
                    }
                >
                    Close me!
        </Alert>
            </Collapse>
        </div>
    </React.Fragment>
}

