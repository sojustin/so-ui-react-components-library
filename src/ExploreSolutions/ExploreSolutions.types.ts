import { MouseEventHandler } from "react";

export interface ExploreSolutionsProps {
    img: string;
    title: string;
    desc: string;
    onClick?: Function;
    isActive?: boolean;
    disabled?: boolean;
}
