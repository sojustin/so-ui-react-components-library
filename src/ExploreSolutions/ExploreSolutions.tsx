import React from "react";

import { ExploreSolutionsProps } from "./ExploreSolutions.types";

import "./ExploreSolutions.scss";
import SOAssetLoader from "../SOAssetLoader/SOAssetLoader";

const ExploreSolutions: React.FC<ExploreSolutionsProps> = ({ img, title, desc, onClick, isActive, disabled }) => (
    <div data-testid="ExploreSolutions" className={`explore-solutions d-flex ${isActive ? 'active' : ''} ${disabled ? 'disabled' : ''}`} onClick={() => !disabled && onClick && onClick()}>
        <SOAssetLoader asset={img} style={{width: 41.9, height: 49.2}} className="image" />
        <div className="pl-3">
            <div className="title">{title}</div>
            <div className="description">{desc}</div>
        </div>
    </div>
);

export default ExploreSolutions;

