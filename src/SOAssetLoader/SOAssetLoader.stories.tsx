import React from "react";
import SOAssetLoader from "./SOAssetLoader";

export default {
    title: "Solve Online/Components/SOAssetLoader",
    component: SOAssetLoader
};

export const WithBar = () => <SOAssetLoader asset="notification-unread.svg" style={{width: 200}}/>;
