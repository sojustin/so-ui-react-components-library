import { CSSProperties } from "react";

export interface SOAssetLoaderProps {
    asset: string;
    className?: string;
    style?: CSSProperties
}
