import React from "react";

import { SOAssetLoaderProps } from "./SOAssetLoader.types";

const SOAssetLoader: React.FC<SOAssetLoaderProps> = ({ asset, className, style }) => (
    <img
        src={`https://business-media.solveonline.com/assets/${asset}`}
        className={className}
        style={style}
    />
);

export default SOAssetLoader;

