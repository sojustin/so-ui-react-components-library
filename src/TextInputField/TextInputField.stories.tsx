import { Formik } from "formik";
import React from "react";
import TextInputField from "./TextInputField";

export default {
    title: "Solve Online/Form/Text Input",
    component: TextInputField
};

// Synchronous validation
const validate = (values /* only available when using withFormik */) => {
    const errors = { text: '' };

    if (!values.text) {
        errors.text = 'Text is required'
    }

    return errors;
};

export const Input = () => (
    <Formik
        initialValues={{ text: '' }}
        validate={validate}
        onSubmit={() => { }}
    >
        {(formProps) => {
            const {
                handleSubmit,
                errors,
                touched,
                values,
                setFieldValue,
                setFieldTouched,
            } = formProps;

            return (
                <>
                    <TextInputField
                        id="text"
                        label="Text"
                        value={values.text}
                        onChange={setFieldValue}
                        onBlur={setFieldTouched}
                        error={errors.text}
                        touched={touched.text}
                        autoComplete="new-text"
                        disabled={false}
                        className=""
                        placeholder="Placeholder text"
                        required={true}
                        helpText="Enter some text"
                    />
                    <TextInputField
                        id="disabled"
                        label="Disabled Input"
                        value=""
                        onChange=""
                        onBlur=""
                        error=""
                        touched=""
                        autoComplete=""
                        disabled={true}
                        className=""
                        inline={true}
                        grid={{ label: 2, field: 4 }}
                    />
                </>
            )
        }}
    </Formik>
);
