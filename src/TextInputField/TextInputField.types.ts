export interface TextInputFieldProps {
    id: string;
    type?: string;
    onChange: any;
    onBlur: any;
    value: any;
    autoComplete?: any;
    className?: string;
    disabled?: any;
    error: any;
    touched: any;
    label: string;
    inline?: boolean,
    grid?: {label: number, field: number};
    placeholder?: string;
    helpText?:string;
    required?:boolean;
}
