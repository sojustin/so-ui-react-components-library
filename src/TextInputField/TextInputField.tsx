import React from "react";
import { Field } from "formik";
import { FormGroup, Label } from "reactstrap";
import { Alert } from '@material-ui/lab'

import { TextInputFieldProps } from "./TextInputField.types";

import "./TextInputField.scss";

const TextInputField: React.FC<TextInputFieldProps> = ({ autoComplete, error, touched, label, id, onChange, onBlur, value, disabled, className, type, inline, grid, placeholder, helpText, required }) => {
    const shouldShowError = !!error && touched;
    const intentLabelClassName = shouldShowError
        ? "text-danger"
        : "text-success";
    const intentFieldClassName = shouldShowError ? "is-invalid" : "is-valid";
    return (
        <FormGroup className={inline && 'row align-items-center'}>
            <Label className={`${touched && intentLabelClassName} ${inline && `col-${grid.label || 2}`}`}>
                {label}
                {required && <sup style={{ color: "red", fontSize: "20px", top: 0 }}>&#42;
                </sup>}
            </Label>
            <div className={inline && `col-${grid.field || 8} input-group` || 'input-group'}>
                <Field
                    id={id}
                    type={type || 'text'}
                    onChange={({ target }) => onChange(id, target.value)}
                    onBlur={() => onBlur(id, true)}
                    value={value}
                    autoComplete={autoComplete}
                    className={`form-control ${touched && intentFieldClassName} ${className}`}
                    disabled={disabled || false}
                    placeholder={placeholder}
                />
                
            </div>
            {
                    helpText &&
                    <p className="my-2" style={{ fontSize: "10px", paddingLeft: "5px", fontStyle: "italic" }}>{helpText}</p>
                }
            {shouldShowError && (
                <div className="errorDiv">
                    <Alert severity="error" className="mb-3">
                        {error}
                    </Alert>
                </div>
            )}
        </FormGroup>
    );
}

export default TextInputField;

