import React from "react";

import { SOCheckBoxProps } from "./SOCheckBox.types";

import "./SOCheckBox.scss";

const SOCheckBox: React.FC<SOCheckBoxProps> = ({ id, className, onChange, checked, label }) => (
    <div className={`custom-checkbox custom-control ${className}`}>
        <input name={id} id={id} type="checkbox" className="custom-control-input" checked={checked} onChange={onChange}/>
        <label className="custom-control-label" htmlFor={id}>{label}</label>
    </div>
);

export default SOCheckBox;

