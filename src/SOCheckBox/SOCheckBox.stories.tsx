import React from "react";
import SOCheckBox from "./SOCheckBox";

export default {
    title: "Solve Online/Input Fields/SOCheckBox",
    component: SOCheckBox
};

export const Checked = () => <SOCheckBox id="checked" checked={true} onChange={event => {alert(`${event.target.name}: ${event.target.checked}`)}} label="Checked"/>;
export const UnChecked = () => <SOCheckBox id="unchecked" onChange={event => {alert(`${event.target.name}: ${event.target.checked}`)}} label="Unchecked"/>;
