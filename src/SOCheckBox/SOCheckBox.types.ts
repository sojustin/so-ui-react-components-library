export interface SOCheckBoxProps {
    className?: string;
    checked?: boolean;
    onChange: (event) => void;
    id: string;
    label: string | React.ReactNode;
}
