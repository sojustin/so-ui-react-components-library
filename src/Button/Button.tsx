import React from "react";

import { ButtonProps } from "./Button.types";

import "./Button.scss";

const Button: React.FC<ButtonProps> = ({ type, disabled, onClick, children, className, size, action }) => (
    <button
        className={`btn btn-${size || 'md'} btn-${type || 'white'} ${className || ''}`}
        onClick={onClick}
        type={action || "button"}
        disabled={!!disabled}
    >{children}</button>
);

export default Button;

