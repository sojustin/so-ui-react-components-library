import React from "react";
import Button from "./Button";

export default {
    title: "Solve Online/Input Fields/Button",
    component: Button
};

export const Buttons = () => <>
    <Button onClick={() => { }} type="white" > White</Button>
    <Button onClick={() => { }} type="primary" >Primary</Button>
    <Button onClick={() => { }} type="dark" >Dark</Button>
    <Button onClick={() => { }} type="secondary" >Secondary</Button>
    <Button onClick={() => { }} type="success" >Success</Button>
    <Button onClick={() => { }} type="warning" >Warning</Button>
    <Button onClick={() => { }} type="info" >Info</Button>
    <Button onClick={() => { }} type="danger" >Danger</Button>
    <Button onClick={() => { }} type="primary" size="xs">Primary xs</Button>
    <Button onClick={() => { }} type="primary" size="sm">Primary sm</Button>
    <Button onClick={() => { }} type="primary" size="lg">Primary lg</Button>
</>;
