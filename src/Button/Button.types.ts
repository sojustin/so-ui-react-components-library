export interface ButtonProps {
    type: 'white' | 'primary' | 'dark' | 'secondary' | 'success' | 'info' | 'warning' | 'danger' | 'neutral';
    disabled?: boolean;
    onClick?: () => void;
    size?: 'xs' | 'sm' | 'md' | 'lg';
    className?: string
    children: React.ReactNode
    action?: "button" | "submit" | "reset"
}
