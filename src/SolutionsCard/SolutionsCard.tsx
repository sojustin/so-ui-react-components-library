import React, { useState } from "react";

import { SolutionsCardProps } from "./SolutionsCard.types";

import "./SolutionsCard.scss";
import SOAssetLoader from "../SOAssetLoader/SOAssetLoader";

const SolutionsCard: React.FC<SolutionsCardProps> = ({ onIconClick }) => {

    const solutionsArray = [
        {
            title: "Marketing",
            imageUrl: "Solve_Online_44x44_M.svg",
        },
        {
            title: "Innovation",
            disabled: true,
            imageUrl: "SOI_disabled.svg",
        },
        {
            title: "Health",
            disabled: true,
            imageUrl: "SOH_disabled.svg",
        },
        {
            title: "Education",
            disabled: true,
            imageUrl: "SOE_disabled.svg",
        },
        // {
        //     title: "Real Estate",
        //     disabled: true,
        //     imageUrl: "SOR_disabled.svg",
        // },
        {
            title: "Finance",
            disabled: true,
            imageUrl: "SOF_disabled.svg",
        },
    ];
    const [currentSolution, setCurrentSolution] = useState(null);
    return (
        <div className="d-flex col-10 row m-auto justify-content-center">
            {solutionsArray.map((solution, index) => {
                let isActive = index === currentSolution;
                return (
                    <div
                        style={{ cursor: "pointer" }}
                        onClick={() => {
                            if (!solution.disabled) {
                                setCurrentSolution(index);
                                onIconClick(solution.title)
                            }
                        }
                        }
                        key={index}
                        className="flex-column align-items-center justify-content-center cursor-pointer d-flex col-4 col-md-1 px-0"
                    >
                        <SOAssetLoader asset={solution.imageUrl} style={{ width: `${isActive ? "60px" : "42px"}` }} ></SOAssetLoader>
                        <p
                            className="solutionText"
                            style={{
                                color: `${isActive ? "#3b78ff" : "#000"}`,
                                fontSize: `${isActive ? "14.7px" : "12px"}`,
                            }}
                        >
                            {solution.title}
                        </p>
                    </div>
                );
            })}
        </div>
    )
};

export default SolutionsCard;

