import React from "react";
import SOSolutionsCard from "./SolutionsCard";

export default {
    title: "Solve Online/Components/SolutionsCard",
    component: SOSolutionsCard
};

export const SolutionsCard = () => <SOSolutionsCard onIconClick={(value) => alert(value)} />;
