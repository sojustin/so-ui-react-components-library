import React from 'react';
import Table from './Table';
import ImageWithText from '../ImageWithText/ImageWithText';

export default {
  title: 'Solve Online/Tables/Billing Table',
  component: Table,
};
const columns = [
  {
    name: 'detailedInfo',
    label: 'DETAILED INFO',
    options: {
      filter: false,
      sort: false,
      filterType: 'dropdown',
      customBodyRender: (value) => <ImageWithText title={value.title} imageUrl={value.ImageUrl} />,
    },
  },
  {
    name: 'createdAt',
    label: 'CREATED AT',
    options: {
      filter: false,
      sort: false,
      customBodyRender: (value) => <p style={{ color: '#9599ac', marginBottom: 0 }}>{value}</p>,
    },
  },
  {
    name: 'price',
    label: 'PRICE',
    options: {
      filter: false,
      sort: false,
      customBodyRender: (value) => <p style={{ color: '#9599ac', marginBottom: 0 }}>{value}</p>,
    },
  },
  {
    name: 'status',
    label: 'STATUS',
    options: {
      filter: false,
      sort: false,
      customBodyRender: (value) => (
        <p style={{ marginBottom: 0, color: `${value === 'SUCCESFUL' ? '#9599ac' : '#11cad1'}` }}>
          {value}
        </p>
      ),
    },
  },
];

const data = [
  {
    detailedInfo: {
      imageUrl: 'https://svgshare.com/i/V1j.svg',
      title: 'Justin Wilson has purchase the leads',
    },
    createdAt: 'A month ago',
    price: '15$',
    status: 'SUCCESFUL',
  },
  {
    detailedInfo: {
      imageUrl: 'https://svgshare.com/i/V1j.svg',
      title: 'Justin Wilson has purchase the leads',
    },
    createdAt: 'A month ago',
    price: '15$',
    status: 'ACTIVE',
  },
  {
    detailedInfo: {
      imageUrl: 'https://svgshare.com/i/V1j.svg',
      title: 'Justin Wilson has purchase the leads',
    },
    createdAt: 'A month ago',
    price: '15$',
    status: 'SUCCESFUL',
  },
  {
    detailedInfo: {
      imageUrl: 'https://svgshare.com/i/V1j.svg',
      title: 'Justin Wilson has purchase the leads',
    },
    createdAt: 'A month ago',
    price: '15$',
    status: 'ACTIVE',
  },
];
const options = {
  download: false,
  print: false,
  viewColumns: false,
  pagination: false,
  filter: false,
  selectableRows: false,
  selectToolbarPlacement: 'none',
  setRowProps: (row, dataIndex, rowIndex) => {
    return {
      className: rowIndex % 2 === 0 ? 'rowBg' : '',
    };
  },
  search: false,
  textLabels: {
    body: {
      noMatch: 'Sorry, no matching records found',
    },
  },
};

export const TableWrapper = () => (
  <Table columns={columns} data={data} title='Billing Statements' options={options} />
);
