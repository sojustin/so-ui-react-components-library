import React, { useEffect, useState } from 'react';
import MUIDataTable from 'mui-datatables';
import { TableProps } from './Table.types';
import './Table.scss';
import ReactPaginate from 'react-paginate';
import CheckBoxOutlineBlankSharpIcon from '@material-ui/icons/CheckBoxOutlineBlankSharp';
import CheckBoxSharpIcon from '@material-ui/icons/CheckBoxSharp';
import { Checkbox } from '@material-ui/core';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
const Table: React.FC<TableProps> = ({
  title,
  data,
  columns,
  options,
  components,
  rowsPerPage = 5,
  customPagination = false,
  changePage,
  pageTotal,
  currentPage
}) => {
  const [localData, setLocalData] = useState([]);
  const [currentData, setCurrentData] = useState([]);
  // setting data for pagination
  options = {
    ...options,
    responsive: 'vertical',
    download: false,
    print: false,
    viewColumns: false,
    pagination: false,
    filter: false,
    selectToolbarPlacement: 'none',
    search: false,
    selectableRowsOnClick: true,
  };
  const customCheckBox = (props) => {
    let newProps = Object.assign({}, props);

    return (
      <Checkbox
        checkedIcon={<CheckBoxSharpIcon />}
        icon={<CheckBoxOutlineBlankSharpIcon />}
        {...newProps}
      />
    );
  };
  components = {
    ...components,
    Checkbox: customCheckBox,
  };
  const getMuiTheme = () =>
    createMuiTheme({
      overrides: {
        MuiPaper: {
          rounded: {
            borderRadius: 0,
          },
          elevation4: {
            boxShadow: 'none',
          },
        },
        MuiTypography: {
          h6: {
            fontFamily: 'Montserrat, sans-serif',
            fontSize: '12.6px',
            fontWeight: 'bold',
          },
        },
        MuiTableCell: {
          head: {
            fontSize: '9.4px',
          },
          body: {
            fontSize: '10.5px',
          },
          paddingCheckbox: {},
        },
      },
    });

  const restructorData = (data, len) => {
    let i = 0,
      size = 0;
    let newData = [];
    let main = [];
    while (i <= data.length) {
      if (size < len) {
        if (data[i] !== undefined) main.push(data[i]);
        size++;
        i++;
      } else {
        size = 0;
        newData.push(main);
        main = [];
      }
    }
    if (main.length !== 0) {
      newData.push(main);
    }
    return newData;
  };
  useEffect(() => {
    setLocalData(restructorData(data, rowsPerPage));
    setCurrentData(
      restructorData(data, rowsPerPage).length ? restructorData(data, rowsPerPage)[0] : [],
    );
  }, [rowsPerPage, data]);
  const handlePageChange = (value) => {
    setCurrentData(localData[value.selected]);
  };

  return (
    <div className='customTable position-relative'>
      <div style={{ boxSizing: 'content-box' }}>
        {currentData && (
          <MuiThemeProvider theme={getMuiTheme()}>
            <MUIDataTable
              title={title}
              data={currentData}
              columns={columns}
              options={options}
              components={components}
            />
          </MuiThemeProvider>
        )}
        {(pageTotal > 1 || localData.length > 1) && <ReactPaginate
          previousLabel={'Prev'}
          nextLabel={'Next'}
          breakLabel={'...'}
          initialPage={(customPagination == true) ? currentPage : 0}
          forcePage={(customPagination == true) ? currentPage : 0}
          pageCount={ (customPagination == true) ? pageTotal : localData.length }
          marginPagesDisplayed={0}
          pageRangeDisplayed={2}
          onPageChange={ (customPagination == true ) ? changePage : handlePageChange }
          containerClassName={'pagination'}
          activeClassName={'pagination__active'}
          disabledClassName={'pagination__disabled'}
          breakClassName={'pagination__break'}
          disableInitialCallback={true}
        />}
      </div>
    </div>
  );
};

export default Table;
