import React from 'react';
import Table from './Table';

export default {
  title: 'Solve Online/Tables/Projects Table',
  component: Table,
};
const columns = [
  {
    name: 'name',
    label: 'NAME',
    options: {
      filter: false,
      sort: false,
      filterType: 'dropdown',
    },
  },
  {
    name: 'type',
    label: 'TYPE',
    options: {
      filter: false,
      sort: false,
      customBodyRender: (value) => <p style={{ marginBottom: 0, color: `#cbd5e0` }}>{value}</p>,
    },
  },
  {
    name: 'status',
    label: 'STATUS',
    options: {
      filter: false,
      sort: false,
      customBodyRender: (value) => (
        <p style={{ marginBottom: 0, color: `${value === 'INACTIVE' ? '#cbd5e0' : '#11cad1'}` }}>
          {value}
        </p>
      ),
    },
  },
  {
    name: 'budget',
    label: 'BUDGET',
    options: {
      filter: false,
      sort: false,
    },
  },

  {
    name: 'balance',
    label: 'BALANCE',
    options: {
      filter: false,
      sort: false,
    },
  },

  {
    name: 'createdAt',
    label: 'CREATED AT',
    options: {
      filter: false,
      sort: false,
    },
  },
];

const data = [
  {
    name: 'Project Name',
    type: 'automated',
    status: 'ACTIVE',
    budget: '1500$',
    balance: '1500$',
    createdAt: 'July 19th 2020 8:35 pm EDT',
  },
  {
    name: 'Project Name',
    type: 'automated',
    status: 'ACTIVE',
    budget: '1500$',
    balance: '1500$',
    createdAt: 'July 19th 2020 8:35 pm EDT',
  },
  {
    name: 'Project Name',
    type: 'automated',
    status: 'ACTIVE',
    budget: '1500$',
    balance: '1500$',
    createdAt: 'July 19th 2020 8:35 pm EDT',
  },
  {
    name: 'Project Name',
    type: 'automated',
    status: 'ACTIVE',
    budget: '1500$',
    balance: '1500$',
    createdAt: 'July 19th 2020 8:35 pm EDT',
  },
  {
    name: 'Project Name',
    type: 'automated',
    status: 'ACTIVE',
    budget: '1500$',
    balance: '1500$',
    createdAt: 'July 19th 2020 8:35 pm EDT',
  },
];
const options = {
  download: false,
  print: false,
  viewColumns: false,
  pagination: false,
  filter: false,
  selectToolbarPlacement: 'none',
  search: false,
  textLabels: {
    body: {
      noMatch: 'Sorry, no matching records found',
    },
  },
};

export const TableWrapper = () => (
  <Table columns={columns} data={data} title='Projects Management' options={options} />
);
