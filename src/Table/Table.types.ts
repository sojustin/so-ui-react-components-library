export interface TableProps {
  rowsPerPage?:number;
  currentPage?:number;
  pageTotal?:number;
  title?: string;
  data: any;
  columns: array;
  options?: object;
  components?:object;
  customPagination?:boolean;
  changePage?: Function;
}
