import React from 'react';
import Table from './Table';
import IconsData from '../IconsTableData/IconsTableData';
import ProgressBar from '../CircularProgressbar/CircularProgressbar';
export default {
  title: 'Solve Online/Tables/Icons table',
  component: Table,
};
const columns = [
  {
    name: 'data',
    label: 'DATA',
    options: {
      filter: false,
      sort: false,
      filterType: 'dropdown',
      customBodyRender: (value) => <IconsData hidePersonIcon={!value.name} hideMailIcon={!value.email} hideCallIcon={!value.phone}/>,
    },
  },
  {
    name: 'rating',
    label: 'RATING',
    options: {
      filter: false,
      sort: false,
      customBodyRender: (value) => <ProgressBar percentage={value} />,
    },
  },
  {
    name: 'price',
    label: 'PRICE',
    options: {
      filter: false,
      sort: false,
    },
  },
];

const data = [
  {
    data: {
      name: ' John Doe',
      email: 'john.doe@doe.com',
      phone: '03155811310',
    },
    rating: 50,
    price: '15$',
  },
  {
    data: {
      name: ' John Doe',
      email: 'john.doe@doe.com',
      phone: '03155811310',
    },
    rating: 50,
    price: '15$',
  },
  {
    data: {
      name:' John Doe',
      email: 'john.doe@doe.com',
      phone: '03155811310',
    },
    rating: 50,
    price: '15$',
  },
];
const options = {
  download: false,
  print: false,
  viewColumns: false,
  pagination: false,
  filter: false,
  selectToolbarPlacement: 'none',
  search: false,
  textLabels: {
    body: {
      noMatch: 'Sorry, no matching records found',
    },
  },
};

export const TableWrapper = () => (
  <Table columns={columns} data={data} title='Table with icons' options={options} />
);
