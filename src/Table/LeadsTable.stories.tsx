import React from 'react';
import Table from './Table';
import DataProvided from '../DataProvided/DataProvided';
import ProgressBar from '../CircularProgressbar/CircularProgressbar';


export default {
  title: 'Solve Online/Tables/Leads Table',
  component: Table,
};
const columns = [
  {
    name: 'dataProvided',
    label: 'DATA PROVIDED',
    options: {
      filter: false,
      sort: false,
      filterType: 'dropdown',
      customBodyRender: (value) => (
        <DataProvided
          name={value.name}
          email={value.email}
          phone={value.phone}
          Address={value.Address}
        />
      ),
    },
  },
  {
    name: 'rating',
    label: 'RATING',
    options: {
      filter: false,
      sort: false,
      customBodyRender: (value) => <ProgressBar percentage={value} />,
    },
  },
  {
    name: 'price',
    label: 'PRICE',
    options: {
      filter: false,
      sort: false,
    },
  },
  {
    name: 'status',
    label: 'STATUS',
    options: {
      filter: false,
      sort: false,
      customBodyRender: (value) => (
        <p style={{ marginBottom: 0, color: `${value === 'INACTIVE' ? '#cbd5e0' : '#11cad1'}` }}>
          {value}
        </p>
      ),
    },
  },

  {
    name: 'createdAt',
    label: 'CREATED AT',
    options: {
      filter: false,
      sort: false,
    },
  },
];

const data = [
  {
    dataProvided: {
      name: ' Name 1',
      email: 'john.doe@doe.com',
      phone: 'phone',
      Address: 'Brazil, Alagoas',
    },
    rating: 50,
    price: '15$',
    status: 'ACTIVE',
    createdAt: 'July 19th 2020 8:35 pm EDT',
  },
  {
    dataProvided: {
      name: ' Name 2',
      email: 'john.doe@doe.com',
      phone: 'phone',
      Address: 'Brazil, Alagoas',
    },
    rating: 50,
    price: '15$',
    status: 'INACTIVE',
    createdAt: 'July 19th 2020 8:35 pm EDT',
  },
  {
    dataProvided: {
      name: ' Name 3',
      email: 'john.doe@doe.com',
      phone: 'phone',
      Address: 'Brazil, Alagoas',
    },
    rating: 50,
    price: '15$',
    status: 'INACTIVE',
    createdAt: 'July 19th 2020 8:35 pm EDT',
  },
  {
    dataProvided: {
      name: ' Name 4',
      email: 'john.doe@doe.com',
      phone: 'phone',
      Address: 'Brazil, Alagoas',
    },
    rating: 50,
    price: '15$',
    status: 'INACTIVE',
    createdAt: 'July 19th 2020 8:35 pm EDT',
  },
  {
    dataProvided: {
      name: ' Name 5',
      email: 'john.doe@doe.com',
      phone: 'phone',
      Address: 'Brazil, Alagoas',
    },
    rating: 50,
    price: '15$',
    status: 'INACTIVE',
    createdAt: 'July 19th 2020 8:35 pm EDT',
  },
  {
    dataProvided: {
      name: ' Name 6',
      email: 'john.doe@doe.com',
      phone: 'phone',
      Address: 'Brazil, Alagoas',
    },
    rating: 50,
    price: '15$',
    status: 'INACTIVE',
    createdAt: 'July 19th 2020 8:35 pm EDT',
  },
  {
    dataProvided: {
      name: ' Name 7',
      email: 'john.doe@doe.com',
      phone: 'phone',
      Address: 'Brazil, Alagoas',
    },
    rating: 50,
    price: '15$',
    status: 'INACTIVE',
    createdAt: 'July 19th 2020 8:35 pm EDT',
  },
  {
    dataProvided: {
      name: ' Name 8',
      email: 'john.doe@doe.com',
      phone: 'phone',
      Address: 'Brazil, Alagoas',
    },
    rating: 50,
    price: '15$',
    status: 'INACTIVE',
    createdAt: 'July 19th 2020 8:35 pm EDT',
  },
  {
    dataProvided: {
      name: ' Name 9',
      email: 'john.doe@doe.com',
      phone: 'phone',
      Address: 'Brazil, Alagoas',
    },
    rating: 50,
    price: '15$',
    status: 'INACTIVE',
    createdAt: 'July 19th 2020 8:35 pm EDT',
  },
  {
    dataProvided: {
      name: ' Name 10',
      email: 'john.doe@doe.com',
      phone: 'phone',
      Address: 'Brazil, Alagoas',
    },
    rating: 50,
    price: '15$',
    status: 'INACTIVE',
    createdAt: 'July 19th 2020 8:35 pm EDT',
  },
  {
    dataProvided: {
      name: ' Name 11',
      email: 'john.doe@doe.com',
      phone: 'phone',
      Address: 'Brazil, Alagoas',
    },
    rating: 50,
    price: '15$',
    status: 'INACTIVE',
    createdAt: 'July 19th 2020 8:35 pm EDT',
  },
];

export const TableWrapper = () => (
  <Table
    columns={columns}
    data={data}
    title='Leads Management'
    
  />
);
