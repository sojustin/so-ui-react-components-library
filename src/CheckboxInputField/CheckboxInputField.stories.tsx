import React from "react";
import CheckboxInputField from "./CheckboxInputField";
import { Formik } from "formik";

export default {
  title: "Solve Online/Form/CheckboxInputField",
  component: CheckboxInputField,
};

export const Checkbox = () => (
  <Formik initialValues={{ checkbox: "" }} onSubmit={() => {}}>
    {(formProps) => {
      const {
        handleSubmit,
        errors,
        touched,
        values,
        setFieldValue,
        setFieldTouched,
      } = formProps;

      return (
        <CheckboxInputField
          id="checkbox"
          label="Do you agree ?"
          error={errors.checkbox}
          touched={touched.checkbox}
          termsLink="/check"
          privacyLink="/privacyLink"
        />
      );
    }}
  </Formik>
);
