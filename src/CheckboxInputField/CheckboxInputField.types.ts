export interface CheckboxInputFieldProps {
  id: any;
  className?: string;
  disabled?: any;
  error: any;
  touched: any;
  label: string;
  acceptTerms?: boolean;
  termsLink?: string;
  privacyLink?: string;
}
