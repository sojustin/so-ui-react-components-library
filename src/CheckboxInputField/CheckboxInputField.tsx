import React from "react";
import { FormGroup, Label } from "reactstrap";
import { Field } from "formik";
import { Alert } from "@material-ui/lab";

import { CheckboxInputFieldProps } from "./CheckboxInputField.types";

import "./CheckboxInputField.scss";

const CheckboxInputField: React.FC<CheckboxInputFieldProps> = ({
  id,
  touched,
  className,
  disabled,
  error,
  label,
  acceptTerms,
  termsLink,
  privacyLink,
}) => {
  const shouldShowError = !!error && touched;
  console.log(acceptTerms, "check accpetterms");
  return (
    <FormGroup className="custom-control custom-checkbox">
      <Field
        id={id}
        type="checkbox"
        name={id}
        className={`custom-control-input ${className || ""}`}
        disabled={disabled || false}
      />
      <label className={`custom-control-label`} htmlFor={id}>
        {!acceptTerms ? (
          label
        ) : (
          <span>
            I have read and agree to the{" "}
            <a
              target="_blank"
              style={{ cursor: "pointer", textDecoration: "underline" }}
              href={termsLink}
            >
              {" "}
              Terms and Conditions
            </a>{" "}
            and{" "}
            <a
              target="_blank"
              style={{ cursor: "pointer", textDecoration: "underline" }}
              href={privacyLink}
            >
              Privacy Policy
            </a>
          </span>
        )}
      </label>
      {shouldShowError && (
        <div className="errorDiv">
          <Alert severity="error" className="mb-3">
            {error}
          </Alert>
        </div>
      )}
    </FormGroup>
  );
};

export default CheckboxInputField;
