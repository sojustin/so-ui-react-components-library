export interface IconsTableData {
  hidePersonIcon?: boolean;
  hideMailIcon?: boolean;
  hideCallIcon?: boolean;
}
