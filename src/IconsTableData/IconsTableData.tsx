import React from 'react';
import { IconsTableData } from './IconsTable.types';
import PersonOutlineIcon from '@material-ui/icons/PersonOutline';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import PhoneEnabledIcon from '@material-ui/icons/PhoneEnabled';
import './IconsTableData.scss';

const TableICons: React.FC<IconsTableData> = ({ hidePersonIcon, hideMailIcon, hideCallIcon }) => {
  return (
    <div className='d-flex align-items-center tableIcons'>
      {!hidePersonIcon && <PersonOutlineIcon fontSize="small"/>}
      {!hideMailIcon && <MailOutlineIcon fontSize="small"/>}
      {!hideCallIcon && <PhoneEnabledIcon fontSize="small"/>}
    </div>
  );
};

export default TableICons;
