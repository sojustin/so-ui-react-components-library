import React from 'react';
import Data from './IconsTableData';

export default {
  title: 'Solve Online/Components/Table Icons',
  component: Data,
};

export const TableIcons = () => (
  <Data/>
);
