import React from "react";
import SOHelpPopover from "./SOHelpPopover";

export default {
    title: "Solve Online/Components/HelpPopover",
    component: SOHelpPopover
};
const body = <div><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p><ul><li>One</li><li>Two</li></ul></div>
export const HelpPopover = () => <SOHelpPopover id="demo" title="How can we help?" body={body}/>;
