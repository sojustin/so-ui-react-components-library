import React, { Fragment, useState } from "react";

import { SOHelpPopoverProps } from "./SOHelpPopover.types";
import HelpIcon from '@material-ui/icons/Help';
import { Popover, PopoverHeader, PopoverBody } from 'reactstrap'

import "./SOHelpPopover.scss";

const SOHelpPopover: React.FC<SOHelpPopoverProps> = ({ id, title, body, placement = 'auto', iconFontSize = 'small', size = 'lg', trigger = 'hover' }) => {

    const [popoverOpen, setPopoverOpen] = useState(false);

    const toggle = () => setPopoverOpen(!popoverOpen);

    return (
        <Fragment>
            <HelpIcon id={id} fontSize={iconFontSize} />
            <Popover placement={placement} isOpen={popoverOpen} target={id} toggle={toggle} trigger={trigger} className={size}>
                <PopoverHeader>{title}</PopoverHeader>
                <PopoverBody>{body}</PopoverBody>
            </Popover>
        </Fragment >
    )
};

export default SOHelpPopover;

