export interface SOHelpPopoverProps {
    id: string;
    title: any;
    body: any;
    placement?: 'auto' | 'auto-start' | 'auto-end' | 'top' | 'top-start' | 'top-end' | 'right' | 'right-start' | 'right-end' | 'bottom' | 'bottom-start' | 'bottom-end' | 'left' | 'left-start' | 'left-end';
    iconFontSize?: 'default' | 'inherit' | 'small' | 'large';
    size?: 'sm' | 'md' | 'lg',
    trigger?: 'click' | 'hover' | 'focus'
}
