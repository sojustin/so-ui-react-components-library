import React from "react";
import { CardTypes } from "./ProjectCard.types";
import "./ProjectCard.scss";
import SOAssetLoader from "../SOAssetLoader/SOAssetLoader";

const ProjectCard: React.FC<CardTypes> = ({
  title,
  date,
  buttonText,
  budget,
  leads,
  status,
}) => {
  return (
    <div
      className={`projectCard d-flex ${status === "active" ? "activeProjectCard" : "InActiveProjectCard"
        }`}
    >
      <div className="productImage">
        <SOAssetLoader asset="Solve_Online_44x44_R.svg" />
      </div>
      <div className="contentSection d-flex align-items-end w-100 justify-content-between">
        <div className="leftContent">
          <h1 className="title mb-1">{title}</h1>
          <div className="details">
            <div
              className={`d-flex mb-1 align-items-center ${!budget ? "opac-0" : ""
                }`}
            >
              <img
                src={
                  "https://so-business-platform-media.s3.amazonaws.com/components-library/Icon_Project_Card_Budget.svg"
                }
                alt="user-friends"
              />
              <h5>
                ${budget} <span>BUDGET</span>
              </h5>
            </div>
            <div className="d-flex">
              {leads && (
                <div className="d-flex align-items-center">
                  <img
                    src={
                      "https://so-business-platform-media.s3.amazonaws.com/components-library/Icon_Project_Card_Leads.svg"
                    }
                    alt="user-friends"
                  />
                  <h5>
                    ${leads} <span className="text">Leads</span>
                  </h5>
                </div>
              )}
              {date && (
                <div
                  className={`d-flex  align-items-center ${leads ? "ml-3" : ""
                    }`}
                >
                  <img
                    src="https://so-business-platform-media.s3.amazonaws.com/components-library/Icon_Project_Card_Calendar.svg"
                    alt="calendar-check-3x"
                  />
                  <h5 className="mb-0"> {date}</h5>
                </div>
              )}
            </div>
          </div>
        </div>
        <div className="button_container">
          {/* active */}
          <button className={`${status === "active" ? "active" : "inActive"}`}>
            {
              buttonText ? buttonText : (status === "active" ? "Active" : " Inactive")
            }
          </button>
        </div>
      </div>
    </div>
  );
};
export default ProjectCard;
