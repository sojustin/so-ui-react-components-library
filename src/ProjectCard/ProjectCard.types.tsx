export interface CardTypes {
  title: String;
  date: any;
  budget?: any;
  leads: any;
  status: String;
  buttonText?:String
}
