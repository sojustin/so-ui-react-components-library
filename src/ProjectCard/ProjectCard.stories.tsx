import React from 'react';
import ProjectCards from './ProjectCard';
export default {
  title: 'Solve Online/Cards/Project Card',
  component: ProjectCards,
};

export const ProjectCard = () => <ProjectCards status='active' title='Project Name' date={' Jan 22. 2021'} budget={2000} leads={20} buttonText="View pipeline"/>;
