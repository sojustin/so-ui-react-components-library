export interface EmailInputFieldProps {
    id: any;
    onChange: any;
    onBlur: any;
    value: any;
    className?: string;
    disabled?: any;
    error: any;
    touched: any;
    label: string;
    placeholder?: string;
    helpText?:string;
    required?:boolean;
}
