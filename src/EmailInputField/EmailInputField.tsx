import React from "react";
import { Field } from "formik";
import { FormGroup, Label } from "reactstrap";
import { Alert } from '@material-ui/lab'

import { EmailInputFieldProps } from "./EmailInputField.types";

import "./EmailInputField.scss";

// Synchronous validation
const validate = value => {
    let errorMessage;
    if (!value) {
        errorMessage = 'Email is required'
    } else if (value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
        errorMessage = 'Invalid email address';
    }
    return errorMessage;
};

const EmailInputField: React.FC<EmailInputFieldProps> = ({ error, touched, label, id, onChange, onBlur, value, disabled, className, placeholder, helpText, required }) => {
    const shouldShowError = !!error && touched;
    const intentLabelClassName = shouldShowError
        ? "text-danger"
        : "text-success";
    const intentFieldClassName = shouldShowError ? "is-invalid" : "is-valid";
    return (
        <FormGroup>
            <Label className={touched && intentLabelClassName}>
                {label} {required && <sup style={{color:"red",fontSize:"20px",top:0}}>&#42;
                </sup>}
            </Label>
            <div className="input-group">
                <Field
                    id={id}
                    name={id}
                    onChange={({ target }) => onChange(id, target.value)}
                    onBlur={() => onBlur(id, true)}
                    value={value}
                    className={`form-control ${touched && intentFieldClassName} ${className}`}
                    disabled={disabled || false}
                    validate={validate}
                    placeholder={placeholder}
                />
            </div>
            {
                helpText &&
                <p className="my-2" style={{ fontSize: "10px", paddingLeft: "2px", fontStyle: "italic" }}>{helpText}</p>
            }

            {shouldShowError && (
                <div className="errorDiv">
                    <Alert severity="error" className="mb-3">
                        {error}
                    </Alert>
                </div>
            )}
        </FormGroup>
    );
}

export default EmailInputField;

