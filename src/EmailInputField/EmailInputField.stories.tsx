import { Formik } from "formik";
import React from "react";
import EmailInputField from "./EmailInputField";

export default {
    title: "Solve Online/Form/Email Input",
    component: EmailInputField
};

export const Email = () => (<Formik
    initialValues={{ email: "" }}
    onSubmit={() => { }}
>
    {(formProps) => {
        const {
            handleSubmit,
            errors,
            touched,
            values,
            setFieldValue,
            setFieldTouched,
        } = formProps;

        return (
                <EmailInputField
                    id="email"
                    label="Email Address"
                    value={values.email}
                    onChange={setFieldValue}
                    onBlur={setFieldTouched}
                    error={errors.email}
                    touched={touched.email}
                    placeholder="abc@xyz.com"
                    helpText="Email that you will use to login"
                    required={true}
                />
        )
    }}
</Formik>);
