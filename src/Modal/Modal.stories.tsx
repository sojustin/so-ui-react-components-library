import React, { useState } from "react";
import MainModal from "./Modal";
import Button from "../Button/Button";
export default {
  title: "Solve Online/Modal/Default",
  component: MainModal,
};

export const Modal = () => {
  const [modal, setModal] = useState(false);
  const toggle = () => {
    setModal(!modal);
  };
  return (
    <div>
      <Button onClick={() => toggle()} type="primary">
        Toggle Modal
      </Button>
      <MainModal isOpen={modal} toggle={toggle} className="modal-lg">
        <p className="modal_example_heading">Modal Example</p>
        <p>
          Lorem Ipsum is simply dummy text of the printing and typesetting
          industry. Lorem Ipsum has been the industry's standard dummy text ever
          since the 1500s, when an unknown printer took a galley of type and
          scrambled it to make a type specimen book. It has survived not only
          five centuries, but also the leap into electronic typesetting,
          remaining essentially unchanged. It was popularised in the 1960s with
          the release of Letraset sheets containing Lorem Ipsum passages, and
          more recently with desktop publishing software like Aldus PageMaker
          including versions of Lorem Ipsum.
        </p>
        <div className="buttons_area">
          <Button onClick={() => toggle()} type="danger">
            Cancel
          </Button>
          <Button onClick={() => toggle()} type="success">
            Submit
          </Button>
        </div>
      </MainModal>
    </div>
  );
};
