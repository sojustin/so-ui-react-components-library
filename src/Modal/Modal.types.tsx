export interface ModalTypes {
  className?: String;
  isOpen: boolean;
  toggle: () => void;
  children?: any;
}
