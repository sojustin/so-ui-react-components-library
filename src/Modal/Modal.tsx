import React from "react";
import { ModalTypes } from "./Modal.types";
import "./Modal.scss";
import { Modal, ModalBody } from "reactstrap";

const Modals: React.FC<ModalTypes> = ({
  className,
  isOpen,
  toggle,
  children,
}) => {
  return (
    <Modal
      isOpen={isOpen}
      toggle={toggle}
      className={`so-modal ${className ? className : ""}`}
    >
      <ModalBody>{children}</ModalBody>
    </Modal>
  );
};
export default Modals;
