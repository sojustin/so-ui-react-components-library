import React from "react";
import PasswordInputField from "./PasswordInputField";
import { Formik } from "formik";

export default {
    title: "Solve Online/Form/Password Input",
    component: PasswordInputField
};

export const Password = () => (
    <Formik
    initialValues={{ password: "" }}
    onSubmit={() => { }}
>
    {(formProps) => {
        const {
            handleSubmit,
            errors,
            touched,
            values,
            setFieldValue,
            setFieldTouched,
        } = formProps;

        return (
                <PasswordInputField
                    id="password"
                    label="Password"
                    value={values.password}
                    onChange={setFieldValue}
                    onBlur={setFieldTouched}
                    error={errors.password}
                    touched={touched.password}
                    disabled={false}
                    className=""
                    required={true}
                    helpText="Enter a password that you will use for login"
                />
        )
    }}
</Formik>
);

