import React from "react";
import { Field } from "formik";
import { FormGroup, Label } from "reactstrap";
import { Alert } from '@material-ui/lab'

import { PasswordInputFieldProps } from "./PasswordInputField.types";

import "./PasswordInputField.scss";

const validate = value => {
    let errorMessage;
    if (!value) {
        errorMessage = 'Password is required'
    }
    return errorMessage;
};

const PasswordInputField: React.FC<PasswordInputFieldProps> = ({ error, touched, label, id, onChange, onBlur, value, disabled, className,helpText, required}) => {
    const shouldShowError = !!error && touched;
    const intentLabelClassName = shouldShowError
        ? "text-danger"
        : "text-success";
    const intentFieldClassName = shouldShowError ? "is-invalid" : "is-valid";
    return (
        <FormGroup>
            <Label className={touched && intentLabelClassName}>
                {label}{required && <sup style={{color:"red",fontSize:"20px",top:0,paddingLeft:"1px"}}>&#42;
                </sup>}
            </Label>
            <div className="input-group">
                <Field
                    id={id}
                    name={id}
                    type="password"
                    onChange={({ target }) => onChange(id, target.value)}
                    onBlur={() => onBlur(id, true)}
                    value={value}
                    className={`form-control ${touched && intentFieldClassName} ${className}`}
                    disabled={disabled || false}
                    validate={validate}
                />
            </div>
            {
                helpText &&
                <p className="my-2" style={{ fontSize: "10px", paddingLeft: "2px", fontStyle: "italic" }}>{helpText}</p>
            }
            {shouldShowError && (
                <div className="errorDiv">
                    <Alert severity="error" className="mb-3">
                        {error}
                    </Alert>
                </div>
            )}
        </FormGroup>
    );
}

export default PasswordInputField;

