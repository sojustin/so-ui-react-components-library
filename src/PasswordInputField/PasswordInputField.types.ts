export interface PasswordInputFieldProps {
    id: any;
    onChange: any;
    onBlur: any;
    value: any;
    className?: string;
    disabled?: any;
    error: any;
    touched: any;
    label: string;
    helpText?:string;
    required?:boolean;
}
