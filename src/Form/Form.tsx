import React from "react";

import { FormProps } from "./Form.types";

import "./Form.scss";
import { Formik } from "formik";
import EmailInputField from '../EmailInputField/EmailInputField';
import PasswordInputField from '../PasswordInputField/PasswordInputField';
import TextInputField from '../TextInputField/TextInputField';
import Button from "../Button/Button";

const Form: React.FC<FormProps> = ({ initialValues, onSubmit, fields, submitButtonLabel, validateSchema, children, enableReinitialize }) => (
    <Formik
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={validateSchema}
        enableReinitialize={enableReinitialize}
    >
        {(formProps) => {
            const {
                handleSubmit,
                errors,
                touched,
                values,
                setFieldValue,
                setFieldTouched,
                isValid
            } = formProps;

            return (
                <form onSubmit={handleSubmit}>
                    {typeof children === 'function' ? children(formProps) :
                        <>
                            {fields.map(field => {
                                if (field.type === 'password') {
                                    return <PasswordInputField
                                        id={field.id}
                                        key={field.id}
                                        label={field.label}
                                        value={values[field.id]}
                                        onChange={setFieldValue}
                                        onBlur={setFieldTouched}
                                        error={errors[field.id]}
                                        touched={touched[field.id]}
                                    ></PasswordInputField>
                                } else if (field.type === 'email') {
                                    return <EmailInputField
                                        id={field.id}
                                        key={field.id}
                                        label={field.label}
                                        value={values[field.id]}
                                        onChange={setFieldValue}
                                        onBlur={setFieldTouched}
                                        error={errors[field.id]}
                                        touched={touched[field.id]}
                                    ></EmailInputField>
                                } else {
                                    return <TextInputField
                                        id={field.id}
                                        key={field.id}
                                        type={field.type}
                                        label={field.label}
                                        value={values[field.id]}
                                        onChange={setFieldValue}
                                        onBlur={setFieldTouched}
                                        error={errors[field.id]}
                                        touched={touched[field.id]}
                                    ></TextInputField>
                                }
                            })}
                            <Button
                                action="submit"
                                type="dark"
                                disabled={!isValid}
                            >{submitButtonLabel}</Button>
                        </>
                    }

                </form>
            )
        }}
    </Formik>
);

export default Form;

