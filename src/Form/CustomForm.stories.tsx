import React from "react";
import Form from "./Form";
import { TextInputField, EmailInputField, PasswordInputField } from ".."
import * as Yup from 'yup';
import Button from "../Button/Button";
import CheckboxInputField from "../CheckboxInputField/CheckboxInputField";
export default {
    title: "Solve Online/Form/With Components",
    component: Form
};
// Validation Logic
const validationSchema = Yup.object().shape({
    name: Yup.string().required('Name is required'),
    email: Yup.string().email('Please Enter a valid Email Address').required('Email Address is required'),
    password: Yup.string().required('Password is required'),
    checkbox: Yup.boolean().oneOf([true], 'Must Accept Terms and Conditions'),
});


export const CustomForm = () => <Form
    initialValues={{ email: 'ab@c.com', password: '', name: 'CP', checkbox: true }}
    onSubmit={(values, actions) => {
        console.log('values', values);
        console.log('actions', actions);
    }}
    validateSchema={validationSchema}
>
    {({ errors, touched, values, setFieldValue, setFieldTouched }) => (
        <>
            <EmailInputField
                id="email"
                label="Email"
                value={values.email}
                onChange={setFieldValue}
                onBlur={setFieldTouched}
                error={errors.email}
                touched={touched.email}
            >
            </EmailInputField>
            <PasswordInputField
                id="password"
                label="Password"
                value={values.password}
                onChange={setFieldValue}
                onBlur={setFieldTouched}
                error={errors.password}
                touched={touched.password}
            ></PasswordInputField>
            <TextInputField
                id="name"
                type="text"
                label="Name"
                value={values.name}
                onChange={setFieldValue}
                onBlur={setFieldTouched}
                error={errors.name}
                touched={touched.name}
            ></TextInputField>
            <CheckboxInputField
                id="checkbox"
                label="Do you agree ?"
                error={errors.checkbox}
                touched={touched.checkbox}
            ></CheckboxInputField>
            <Button
                action="submit"
                type="primary"
            >SignUp</Button>
        </>
    )}
</Form>;

