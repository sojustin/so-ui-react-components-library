interface FieldProps {
    id: string,
    label: string,
    type: string,
    className?: string,
    disabled?: boolean
}

export interface FormProps {
    initialValues: {};
    onSubmit: (values: any, actions: any) => void,
    fields?: FieldProps[],
    submitButtonLabel?: string
    validateSchema?: (values: any) => void
    children?: (formProps: any) => React.ReactNode
    enableReinitialize?: boolean
}
