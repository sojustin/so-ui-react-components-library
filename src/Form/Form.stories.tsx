import React from "react";
import Form from "./Form";
import { TextInputField, EmailInputField, PasswordInputField } from ".."
export default {
    title: "Solve Online/Form/With field list",
    component: Form
};

export const CustomForm = () => <Form
    initialValues={{ email: 'ab@c.com', password: '', name: 'CP' }}
    onSubmit={(values, actions) => {
        console.log('values', values);
        console.log('actions', actions);
    }}
    fields={[
        {
            id: 'name',
            type: 'text',
            label: 'Name',
        }, {
            id: 'email',
            type: 'email',
            label: 'Email',
        }, {
            id: 'password',
            type: 'password',
            label: 'Password',
        }
    ]}
    submitButtonLabel="Submit Storybook"
/>;

