import React from 'react';
import Data from './DataProvided';

export default {
  title: 'Solve Online/Components/Data Provided',
  component: Data,
};

export const DataProvided = () => (
  <Data
    name="John Doe"
    email="john.doe@doe.com"
    phone="+1-541-754-3010"
    Address="Brazil, Alagoas"
  />
);
