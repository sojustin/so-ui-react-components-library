import React from 'react';

import { DataProvidedtypes } from './Dataprovided.types';

import './DataProvided.scss';

const DataProvided: React.FC<DataProvidedtypes> = ({ name, email, phone, Address, width = 300 }) => {

  return (
    <div className='d-flex dataprovidedWrapper' style={{ width }}>
      <div className='dataprovidedWrapper__leftSide' style={{ width: '52%' }}>
        <b className=' dataprovidedWrapper__leftSide__bold'>{name ? name : ''}</b>
        <p className=' dataprovidedWrapper__leftSide__light'>{email ? email : ''}</p>
      </div>
      <div className='dataprovidedWrapper__rightSide' style={{ width: '48%' }}>
        <b className=' dataprovidedWrapper__leftSide__bold'>{phone ? phone : ''}</b>
        <p className=' dataprovidedWrapper__leftSide__light'>{Address ? Address : ''}</p>
      </div>
    </div>
  );
};

export default DataProvided;
