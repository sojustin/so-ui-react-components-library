import React from 'react';

import { ImageWithTextTypes } from './ImageWithText.types';

import './ImageWithText.scss';

const ImageWithText: React.FC<ImageWithTextTypes> = ({ title, imageUrl }) => {
  return (
    <div className='d-flex align-items-center ImageWithTextWrapper'>
      <div
        className='ImageWithTextWrapper__Image'
        style={{
          backgroundImage: `url(${imageUrl ? imageUrl : 'https://svgshare.com/i/V1j.svg'})`,
        }}
      />
      <p className="ImageWithTextWrapper__title">{title}</p>
    </div>
  );
};

export default ImageWithText;
