export interface SelectInputFieldProps {
  id: string;
  options: string[];
  label: string;
  inline?: boolean;
  grid?: { label: number; field: number };
  className?: String;
  onChange?: Function;
  onBlur?: any;
  error?: any;
  touched?: any;
  helpText?:string;
  required?:boolean;
}
