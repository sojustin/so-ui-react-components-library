import React from "react";
import { Button, Form, FormGroup, Label, Input, FormText } from "reactstrap";
import { SelectInputFieldProps } from "./SelectInputField.types";
import { Alert } from "@material-ui/lab";

import "./SelectInputField.scss";
import { Field } from "formik";

const SelectInputField: React.FC<SelectInputFieldProps> = ({
  id,
  options,
  label,
  inline,
  grid,
  onChange,
  error,
  touched,
  className,
  onBlur,
  helpText,
  required
}) => {
  const shouldShowError = !!error && touched;
  const intentLabelClassName = shouldShowError ? "text-danger" : "text-success";
  const intentFieldClassName = shouldShowError ? "is-invalid" : "is-valid";

  return (
    <FormGroup className={inline && "row align-items-center"}>
      <Label className={`${touched && intentLabelClassName}` + inline && `col-${grid.label || 2}`}>{label}{required && <sup style={{ color: "red", fontSize: "20px", top: 0 }}>&#42;
      </sup>}</Label>
      <div className={(inline && `col-${grid.field || 8} input-group`) || "input-group"}>
        <Field
          onChange={({ target }) => onChange && onChange(id, target.value)}
          onBlur={() => onBlur && onBlur(id, true)}
          as="select"
          name={id}
          className={`form-control ${touched && intentFieldClassName} ${className}`}
        >
          <option disabled value="">
            Please select
          </option>
          {options.map((option) => (
            <option value={option}>{option}</option>
          ))}
        </Field>

      </div>
      {
        helpText &&
        <p className="my-2" style={{ fontSize: "10px", paddingLeft: "17px", fontStyle: "italic" }}>{helpText}</p>
      }
      {shouldShowError && (
        <div className="errorDiv col-12">
          <Alert severity="error" className="mb-3">
            {error}
          </Alert>
        </div>
      )}
    </FormGroup>
  );
};

export default SelectInputField;
