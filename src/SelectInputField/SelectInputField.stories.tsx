import { Formik } from "formik";
import React, { useState } from "react";
import SelectInputField from "./SelectInputField";

export default {
  title: "Solve Online/Form/Select Input",
  component: SelectInputField,
};

export const Select = () => {
  const [values, setValues] = useState({ animals: "Cat" })

  return <Formik initialValues={values} onSubmit={() => { }}>
    {({ errors, touched, values, setFieldValue, setFieldTouched, }) => (
      <SelectInputField
        id="animals"
        options={["Cat", "Dog"]}
        label="Animals"
        inline={true}
        grid={{ label: 12, field: 12 }}
        error={errors.animals}
        touched={touched.animals}
        onChange={setFieldValue}
        onBlur={setFieldTouched}
        required={true}
        helpText="Please choose one option"
      ></SelectInputField>
    )}
  </Formik >
};
