import React from 'react';
import Bar from './CircularProgressbar';

export default {
  title: 'Solve Online/Components/Circular Bar',
  component: Bar,
};

export const DataProvided = () => (
  <Bar
    percentage={10}
  />
);
