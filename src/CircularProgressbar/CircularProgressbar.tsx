import React from 'react';

import { CircularBartypes } from './CircularProgressbar.types';
import './CircularProgressbar.scss';

const CircularBar: React.FC<CircularBartypes> = ({ percentage }) => {
  const width = 200;
  const reduction = 0;
  const progress = percentage ? percentage : 0;
  const strokeWidth = 25;
  const ballStrokeWidth = 16;
  const background = '#dde2e9';
  const transitionDuration = 0.5;
  const transitionTimingFunction = 'ease';
  const center = width / 2;
  const height = 200 || center + center * Math.cos(reduction * Math.PI);
  const [unique] = React.useState(() => Math.random().toString());
  const rotate = 90 + 180 * reduction;
  const r = center - strokeWidth / 2 - (ballStrokeWidth / 2);
  const circumference = Math.PI * r * 2;
  const offset = circumference * (100 - progress * (1 - reduction)) / 100;
  const gradient = [{stop: 0.0, color: '#a8c0f4'}, {stop: 1, color: '#a8c0f4'}];
  return (
    <div style={{ height: '40.5px', width: '40.5px' }} className="circular-progress">
      <svg viewBox={`0 0 ${width} ${height}`}>
        <defs>
          <linearGradient id={"gradient" + unique} x1="0%" y1="0%" x2="0%" y2="100%">
            {gradient.map(({ stop, color }) => <stop key={stop} offset={stop * 100 + "%"} stopColor={color} />)}
          </linearGradient>
        </defs>
        <text x={100} y={115} textAnchor="middle">
          {progress}%
          </text>
        <circle
          transform={`rotate(${rotate} ${center} ${center})`}
          id="path"
          cx={center}
          cy={center}
          r={r}
          strokeWidth={strokeWidth}
          strokeDasharray={circumference}
          strokeDashoffset={circumference * reduction}
          fill="none"
          stroke={background}
          strokeLinecap="round">
        </circle>
        <circle
          style={{ transition: `stroke-dashoffset ${transitionDuration}s ${transitionTimingFunction}` }}
          transform={`rotate(${rotate} ${center} ${center})`}
          id="path"
          cx={center}
          cy={center}
          r={r}
          strokeWidth={strokeWidth}
          strokeDasharray={`${circumference}`}
          strokeDashoffset={offset}
          fill="none"
          stroke={`url(#gradient${unique})`}
          strokeLinecap="round">
        </circle>
        <circle
          style={{ transition: `stroke-dashoffset ${transitionDuration}s ${transitionTimingFunction}` }}
          transform={`rotate(${rotate} ${center} ${center})`}
          id="path"
          cx={center}
          cy={center}
          r={r}
          strokeWidth={ballStrokeWidth}
          strokeDasharray={`1 ${circumference}`}
          strokeDashoffset={offset}
          fill="none"
          stroke={`url(#gradient${unique})`}
          strokeLinecap="round">
        </circle>
      </svg>
    </div>
  );
};

export default CircularBar;
